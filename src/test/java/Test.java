import org.junit.*;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;

public class Test {
    @Before
    public void beforeClass(){
        System.out.println("BeforeClass");
    }

    @org.junit.Test
    public void testInt(){
        ForTest tst = new ForTest();
        assertNotNull(tst.a);
    }

    @org.junit.Test
    @Ignore
    public void testIgn(){
        fail();
    }

    @org.junit.Test
    public void testFinal(){
        ForTest tst = new ForTest();
        int x = tst.b + 1;
        System.out.println(x);
    }

    @After
    public void testAfter(){
        System.out.println("AfterClass");
    }
}
